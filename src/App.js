import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import Button from './components/Button';
const App: () => React$Node = () => {
  return (
    <View style={styles.wrapper}>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.scrollView}>
        <ScrollView>
          <View style={styles.sectionContainer}>
            <View style={styles.section}>
              <Text style={styles.boldText}>Single Health</Text>
              <View style={styles.sectionnew}>
                <View>
                  <Text>What you pay</Text>
                  <Text style={styles.boldText}>VND 800 ngan</Text>
                </View>
                <View>
                  <Text>Protection amount</Text>
                  <Text style={styles.boldText}>VND 80 trieu</Text>
                </View>
              </View>
              <View style={styles.sectionnew}>
                <View>
                  <Text>Insurer</Text>
                  <Text style={styles.boldText}>Bao Minh</Text>
                  <Text style={styles.boldText}>Corporation</Text>
                </View>
                <View>
                  <Text>Amount of time</Text>
                  <Text>you are covered</Text>
                  <Text style={styles.boldText}>1 year</Text>
                </View>
              </View>
            </View>
            <View style={styles.sectionview}>
              <View style={styles.secetionSub}>
                <Text style={styles.boldSubText}>What is included</Text>
                <Text>Hospital costs</Text>
                <Text>VND 2 million/ day,</Text>
                <Text>max. of VND 80 million</Text>
              </View>
              <View style={styles.secetionSub}>
                <Text style={styles.boldSubText}>Surgery costs</Text>
                <Text>VND 40 million/ year</Text>
              </View>
              <View style={styles.secetionSub}>
                <Text style={styles.boldSubText}>Money in case of death</Text>
                <Text>Max. of VND 80 million</Text>
              </View>
              <View style={styles.secetionSub}>
                <Text style={styles.boldSubText}>Money in case</Text>
                <Text style={styles.boldSubText}>Total disability</Text>
                <Text>Max. of VND 40 million</Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    width: '100%',
    backgroundColor: 'rgb(3, 168, 124)',
  },
  section: {
    borderRadius: 6,
    borderBottomWidth: 1,
    overflow: 'hidden',
    borderStyle: 'solid',
    borderColor: 'black',
    padding: 15,
    flexDirection: 'column',
  },
  sectionnew: {
    overflow: 'hidden',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  secetionSub: {
    overflow: 'hidden',
    flexDirection: 'column',
    alignItems: 'center',
    margin: 20,
    justifyContent: 'space-between',
  },
  sectionview: {
    overflow: 'hidden',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  boldText: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  boldSubText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  sectionContainer: {
    width: '90%',
    height: 600,
    marginTop: '10%',
    marginLeft: 'auto',
    marginRight: 'auto',
    borderRadius: 6,
    backgroundColor: '#ccc',
    overflow: 'hidden',
    padding: 15,
    flexDirection: 'column',
  },
});

export default App;
